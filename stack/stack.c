/*
Gruppe: 22

Nirvan Charansurya Udaysingh Jhurree
Marc-Jan Szpineter
Sheung Tung Tony Tam

------------------
*/

#include <stdio.h> // fprintf(), ...
#include <stddef.h> // NULL
#include <stdlib.h> // malloc(), realloc(), ...

#include "stack.h"

#define STACK_INIT_CAPACITY_IN_UNITS 1024 // change to 1 to test if realloc() is implemented correctly
#define STACK_ADD_WHEN_FULL_IN_UNITS 1024 // ...
#define SIZE_OF_UNIT (sizeof (int))

#define ERR_ALLOCATING_MEM -1
#define ERR_VAR_IS_NULL -2
#define ERR_POP_ON_EMPTY_STACK -3
#define ERR_TOP_OF_EMPTY_STACK -4

int stackInit(IntStack *self) {

	if (self == NULL)
		return ERR_VAR_IS_NULL;

	self->capacity_in_units = STACK_INIT_CAPACITY_IN_UNITS;
	self->size_in_units = 0;

	self->base = malloc(self->capacity_in_units * SIZE_OF_UNIT);

	if (self->base == NULL) {
		fprintf(stderr, "ERROR during initilalization of stack: received NULL pointer from malloc()\n");
		return ERR_ALLOCATING_MEM;
	}
	else
		return 0;

}

void stackRelease(IntStack *self) {

	if (self != NULL) {
		free(self->base);
	}

}

void stackPush(IntStack *self, int i) {

	// realloc()
	if (self->size_in_units >= self->capacity_in_units) {

		int *new_base = realloc(self->base, (self->capacity_in_units + STACK_ADD_WHEN_FULL_IN_UNITS) * SIZE_OF_UNIT);

		if (new_base == NULL) {
			free(self->base);
			fprintf(stderr, "ERROR while pushing on stack: realloc() out of memory or other reason for NULL\n");
			exit(ERR_ALLOCATING_MEM);
		} else {
			self->capacity_in_units += STACK_ADD_WHEN_FULL_IN_UNITS;
			self->base = new_base;
		}

	}

	*(self->base + self->size_in_units) = i; // *top = i;
	self->size_in_units++;

}

int stackTop(const IntStack *self) {
	
	if (self->size_in_units != 0) {
		return *(self->base + self->size_in_units -1);
	} else {
		fprintf(stderr, "ERROR while requesting top of stack: stack is empty!\n");
		exit(ERR_TOP_OF_EMPTY_STACK);
	}

}

int stackPop(IntStack *self) {

	if (self->size_in_units != 0) {
		self->size_in_units--;
		return *(self->base + self->size_in_units);
	} else {
		fprintf(stderr, "ERROR while popping from stack: stack is empty!\n");
		exit(ERR_POP_ON_EMPTY_STACK);
	}

}

int stackIsEmpty(const IntStack *self) {

	return self->size_in_units <= 0;

}

void stackPrint(const IntStack *self) {

	int curr_size = self->size_in_units;

	printf("<<content of stack>>:\n");
	if (curr_size == 0)
		printf("<<stack is empty>>\n");
	else {
		while (curr_size > 0) {
			printf("%d\n", *(self->base + curr_size -1));
			curr_size--;
		}
	}

}
